/**
 * this class will hold the parameters
 * that we use to search for specific clinics
 */
export class ClinicSearchParams{
    constructor(public doctorId: string, public area: string){}
}