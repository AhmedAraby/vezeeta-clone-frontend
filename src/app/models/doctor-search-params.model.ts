export class DoctorSearchParamsModel{
    constructor(
        public name: string,
        public area: string,
        public speciality: string)
    {}
}