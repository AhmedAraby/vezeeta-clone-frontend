
import { FormGroup } from "@angular/forms";
import { Appointment } from "./appointment.model";

/**
 * this is a database construct 
 * and it represents the schedule for of a specific weekday 
 * for a specific clinic.
 * 
 * openFrom & closeAt has the form "hh:mm:ss"
 * appointmentSpan is in minutes
 */

export class Schedule
{
    public constructor(
        public openFrom: string, public closeAt: string,
        public appointmentSpan: number, public weekday?: string, 
        public clinicId?: string, public id?:string){}

    
    static createClinicSchedule(clinicSchedule)
    : Schedule
    {

        return new Schedule(
            clinicSchedule['openFrom'], 
            clinicSchedule['closeAt'], 
            clinicSchedule['appointmentSpan'],
            clinicSchedule['weekday'], 
            clinicSchedule['clinicId'], 
            clinicSchedule['id'], 
        )
    }

    static createClinicSchedule_local(form:FormGroup)
    :Schedule
    {
        const values = form.value;
        return new Schedule(
            values['openFrom'] + ":00", 
            values['closeAt'] + ":00",
            values['appointmentSpan'], 
        );
    }

    // get Date object from the startAt time    
    getStart(){
        let start = new Date();
        let openParts = this.openFrom.split(":");
        start.setHours(
            parseInt(openParts[0]),  // hours 
            parseInt(openParts[1]),  // minutes
            parseInt(openParts[2])); // seconds
        return start;
    }
    
    // get Date object from the closeAt time
    getClose(){
        let close = new Date();
        let closeParts = this.closeAt.split(":");
        close.setHours(
            parseInt(closeParts[0]),
            parseInt(closeParts[1]), 
            parseInt(closeParts[2]));
        return close;
    }

    /**
     * this method will convert the ClinicSchedule object 
     * to list of appointments 
    */
    getAppointments() : Appointment[]
    {
        // JS Date Object is used becasue it make the 
        // Date manipulation easier
        // we only care about the Time part of a Date object.
        let start = this.getStart();
        let close = this.getClose();
        
        let appointments: Appointment[] = [];

        while(start < close){
            appointments.push(new Appointment(
                start.getHours(),
                start.getMinutes()));
            // get the next appointment, 1 minute = 60000 millie second
            start = new Date(
                start.getTime() +
                this.appointmentSpan * 60000); 
        }
        return appointments;
    }
}