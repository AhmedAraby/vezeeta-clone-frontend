export class PaginationInfo{
    constructor(public pageNum: number, public pageSize: number){}
}