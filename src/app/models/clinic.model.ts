import { FormGroup } from "@angular/forms";

export class Clinic{
    constructor(
        public name: string, public location: string,  // location is coresponding to area
        public address: string, public doctorId: string,
        public imageUrl?: string, public id?: string){}

    static createClinic(clinic) : Clinic
    {
        return new Clinic(
            clinic.name, clinic.location,
            clinic.address, clinic.doctorId, 
            clinic.imageUrl, clinic.id);
    }

    static createClinic_local(form: FormGroup): Clinic
    {
        // values with '""' will be edited from the client code that called this method.
        const values = form.value; 
        return new Clinic(
            values['name'], values['location'], 
            values['address'], null,
        );
    }
}