import { FormGroup } from "@angular/forms";

/** 
 * modeling of the doctor data 
 * from the signup form and from the database 
 * */
export class Doctor
{
    constructor(
        public name:string, public email:string, 
        public phone: string, public speciality: string,
        public education: string,
        public fees: number, public photoUrl:string, 
        public id?: string, public password?: string, 
        public newPassword?:string, public newPasswordConfirm?:string){}
    
    /**
     * 
     * @param doctor  JS Object that have similar properties like Doctor class 
     * @returns new instance of type Doctor
     */
    static createDoctor(doctor: Doctor) : Doctor
    {
        return new Doctor(
            doctor.name, doctor.email,
            doctor.phone, doctor.speciality,
            doctor.education, doctor.fees, doctor.photoUrl, doctor.id);
    }

    static createDoctor_local(form: FormGroup){
        const values = form.value;
        return new Doctor(
            values['name'], values['email'],
            values['phone'], values['speciality'], 
            values['education'], values['fees'], 
            "", "", // id, photo url 
            values['password'], 
            values['newPassword'] || "" , values['newPasswordConfirm'] || "",
        );
    }
}