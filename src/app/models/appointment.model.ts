/**
 * this is not a database construct 
 * it's intent is to aid the frontEnd logic. 
 * 
 * it only works with 24 hours system.
 */

import { DateUtils } from "../utils/dateUtils";

export class Appointment
{
    constructor(public hours: number, public minutes: number){}
    
    getStartAt(){
        // return string representation for 24 hours system 
        return DateUtils.make2Digits(this.hours) + 
        ":" +
        DateUtils.make2Digits(this.minutes) + 
        ":00"; // seconds
    }

}