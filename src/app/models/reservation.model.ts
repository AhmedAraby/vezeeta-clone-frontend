/**
 * this is a database construct 
 * and it represent the reservation 
 * of a specific clinic, and for a specific point in time (time and date)
 */

export class Reservation
{

    constructor(
        public startAt:string, public endAt:string,
        public date:string, public clinicId: string,
        public clientId: string, public id?: string){}
    
    /**
     * 
     * @param reservation is a raw JS Object
     * @returns instance of Reservation class
     */
    static createReservation(reservation: Reservation){
        return new Reservation(
            reservation.startAt, reservation.endAt, 
            reservation.date, reservation.clinicId, 
            reservation.clientId, reservation.id);
    }

}