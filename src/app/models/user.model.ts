/**
 * this model aimed to hold minimual data, 
 * 1- to enable the auth process for the http requests 
 * 2- retrive the loged in user data when needed.
 */
 
export class User 
{
    userType: string;  // client or doctor
    id: number; // data base Id
    jwt: string; 

    constructor(id: number, jwt: string, userType: string)
    {
        this.id = id;
        this.jwt = jwt;
        this.userType = userType;
    }
    
    static createUser(userObj): User
    {
        const id = userObj['id'];
        const jwt = userObj['jwt'];
        const userType = userObj["userType"];
        return new User(id, jwt, userType);
    }
}