/** 
 * modeling of the client data 
 * from the signup form and from the database 
 * */
export class Client
{
    // id comes from the Database
    constructor(
        public name: String, public email: String,
        public phone: String, public password?: string){}
}