/**
 * this service will 
 * 1- handle the login / client-signup / doctor-signup
 *    talking to the backend apis to authenticate / register the user
 * 2- it will change the state of the app to be in the loged in state if the user logedin 
 * 3- or redirect the user to login page after successfule signup
 * 4- handle logout 
 */

import { Injectable, OnInit } from '@angular/core';
import {HttpClient, HttpResponse} from "@angular/common/http";
import { BehaviorSubject, Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { Client } from 'src/app/models/client.model';
import { Doctor } from 'src/app/models/doctor.model';
import { User } from 'src/app/models/user.model';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // signin endpoints
  clientSignInApi: string = "http://localhost:8050/authenticate/login/client";
  doctorSignInApi: string = "http://localhost:8050/authenticate/login/doctor";
  
  // signup endpoints
  clientSignUpApi: string = "http://localhost:8050/authenticate/signup/client";
  doctorSignUpApi: string = "http://localhost:8050/authenticate/signup/doctor";
  
  userStateUpdate: BehaviorSubject<User|null> = new BehaviorSubject(null);
  
  constructor(private httpClient: HttpClient, private router:Router) { 
    this.autoLogIn();
  }

  clientSignIn(email: string, password: string) : Observable<any> // type in the <> means the data emited from the observable
  {
    let observable = 
    this.httpClient
    .post(this.clientSignInApi, {email, password}, 
      {observe:"response", responseType:"json"})
    .pipe(tap((resp)=>{
      if(resp.status == 200){
        const body = resp.body;
        const jwt = body['jwt'];
        const id = body['id'];
        const userType = "client";
        const user = new User(id, jwt, userType);
        this.logIn(user);
      }
    }));
    return observable;                          
  }

  doctorSignIn(email: string, password: string) : Observable<any>
  {
    let observable = 
    this.httpClient
    .post(this.doctorSignInApi, {email, password}, 
      {observe:"response", responseType:"json"})
    .pipe(tap((resp)=>{
      if(resp.status == 200){
        const body = resp.body;
        const jwt = body['jwt'];
        const id = body['id'];
        const userType = "doctor";
        const user = new User(id, jwt, userType);
        this.logIn(user);
      }
    }));

    return observable;                        
  }

  clientSignUp(client: Client) : Observable<any>
  {
    // console.log("signup client is : ", client);

    const observable = this.httpClient
    .post(this.clientSignUpApi, client, 
          {observe:"response", responseType:"json"})
    return observable;  
  }

  doctorSignUp(doctor: Doctor) : Observable<any>
  {
    const observable = this.httpClient
    .post(this.doctorSignUpApi, doctor, 
          {observe:"response", responseType:"json"})
    return observable;
  }


  // signOut(): Observable<void>
  // {

  // }


  // automted work
  logOut()
  {
    localStorage.removeItem("user");
    this.emitUserStateChange(null);
    this.router.navigate(["signin"]);
  }

  logIn(user: User)
  {
    // console.log("user is loged in : ", user);
    localStorage.setItem("user", JSON.stringify(user)); // handle browser refresh
    this.emitUserStateChange(user); // tell interested parties about the login event
  }

  autoLogIn(){
    let userStr = localStorage.getItem('user');
    if(userStr){
      let userObj = JSON.parse(userStr);
      const user:User = User.createUser(userObj);
      this.logIn(user);
    }
  }


  getClientId(): string
  {
    /**
     * only client is allowed to view home pages and make resevations
     */
    const user = this.userStateUpdate.value;
    if(user == null)
      throw new Error("Client is null");
    if(user.userType !="client")
      throw new Error("user is not a client");
    
    return user.id.toString();
  }

  getDoctorId(): string
  {
    /**
     * only doctor is allowed to view pages that manage doctor 
     * account hence callng this method
     */
    const user = this.userStateUpdate.value;
    if(user == null)
      throw new Error("Doctor is null");
    if(user.userType !="doctor")
      throw new Error("user is not a doctor");    
    return user.id.toString();
  }

  getUserType(): string
  {
    const user = this.userStateUpdate.value;
    if(user == null)
      throw new Error("user is null");
    return user.userType;
  }

  getJwt() :string
  {
    if(this.userStateUpdate.value == null)
      return null;
      
    return this.userStateUpdate.value.jwt;
  }
  // event emitters
  emitUserStateChange(user:User)
  {
    this.userStateUpdate.next(user);
  }
}
