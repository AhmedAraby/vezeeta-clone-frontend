import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class AuthInterceptorInterceptor implements HttpInterceptor {

  constructor(
    private authService: AuthService,
    private router: Router) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const jwt =  this.authService.getJwt();
    let newRequest = request;
    if(jwt !=null){
      const authHeader = "Bearer " + jwt;
      newRequest = request.clone({setHeaders:{Authorization:authHeader}});
    }
    return next.handle(newRequest).pipe(
      tap(
        ()=>{
          // not interested in this case
        },

        (resp)=>{
          console.log("response in interceptor is : ", resp);
          if(resp.status == 401)  // unauthorized
            this.authService.logOut();
          return ;
      }),
    );
  }
}
