import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Client } from 'src/app/models/client.model';
import { AuthService } from 'src/app/Authentication/services/auth.service';
import { Router } from '@angular/router';
import { timeout } from 'rxjs/operators';

@Component({
  selector: 'app-client-signup',
  templateUrl: './client-signup.component.html',
  styleUrls: ['./client-signup.component.css']
})
export class ClientSignupComponent implements OnInit {
  form: FormGroup;
  errorMsg: string = "";
  successMsg: string = "";
  
  // patterns
  
  // phone
  phonePattern ="^0+[0-9]{10}"; // egyption phone number
  // password
  passwordP_oneOrMoreSmallChar = ".*[a-z]+.*";
  passwordP_oneOrMoreCapChar = ".*[A-Z]+.*";
  passwordP_oneOrMoreNumber = ".*[0-9]+.*";

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      "name": new FormControl("", [Validators.required]),
      "phone": new FormControl("", 
      [
        Validators.required,
        Validators.pattern(this.phonePattern), 
      ]),  
      
      "email": new FormControl("",[Validators.required, Validators.email]), 
      "password": new FormControl("", 
        [
          Validators.required, 
          Validators.minLength(10), 
          Validators.pattern(this.passwordP_oneOrMoreSmallChar),
          Validators.pattern(this.passwordP_oneOrMoreCapChar), 
          Validators.pattern(this.passwordP_oneOrMoreNumber),
        ])
    });
  }

  onSignup()
  {
    if(this.form.invalid)
      return ;
    
    const values = this.form.value;
    const client = 
      new Client(values['name'], values['email'],
       values['phone'], values['password']);
                
    const observable = this.authService.clientSignUp(client);
    observable.subscribe(
      (resp)=>{
        if(resp.status == 201){
          this.errorMsg = "";
          this.successMsg = "Successful SignUp";
          setTimeout(() => {
            this.router.navigate(["/signin"]);
          }, 2000);
        }
        else{
          this.successMsg = "";
          this.errorMsg = "server error please try again latter";
        }
      },

      (resp)=>{
        this.successMsg = "";
        console.log("error resp is " , resp);
        
        if(resp.status == 400){
          this.errorMsg = resp.error.message;
        }
        else if(resp.error.status == 500) {
          this.errorMsg = "server error please try again latter";
        }
        else{  // status = 0
          this.errorMsg = "network error";
        }

      });
  }

  isInvalidElement(name:string){
    // console.log("form is ", this.form);
    return this.form.controls[name].invalid && this.form.controls[name].touched;
  }

}
