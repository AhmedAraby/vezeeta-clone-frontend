import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Doctor } from 'src/app/models/doctor.model';
import { AuthService } from 'src/app/Authentication/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-doctor-signup',
  templateUrl: './doctor-signup.component.html',
  styleUrls: ['./doctor-signup.component.css']
})
export class DoctorSignupComponent implements OnInit {
  form: FormGroup;
  errorMsg: string = "";
  successMsg: string = "";

  // patterns

  phonePattern:string = "^0{1}[0-9]{10}$"; // egyption phone, regex behave in different way, it allowed strange behaviour !!!????
  feesPattern:string = "^[1-9]+[0-9]*";
  // password
  passwordP_oneOrMoreSmallChar = ".*[a-z]+.*";
  passwordP_oneOrMoreCapChar = ".*[A-Z]+.*";
  passwordP_oneOrMoreNumber = ".*[0-9]+.*";


  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      "name": new FormControl("", [Validators.required]),
      "phone": new FormControl("", [Validators.required, Validators.pattern(this.phonePattern)]),
      "email": new FormControl("", [Validators.required, Validators.email]), 
      "password": new FormControl("", 
      [
        Validators.required, 
        Validators.minLength(10), 
        Validators.pattern(this.passwordP_oneOrMoreSmallChar),
        Validators.pattern(this.passwordP_oneOrMoreCapChar), 
        Validators.pattern(this.passwordP_oneOrMoreNumber),
      ]), 
      "education": new FormControl("", [Validators.required]),
      "speciality": new FormControl("", [Validators.required]),
      "fees": new FormControl("", [Validators.required, Validators.pattern(this.feesPattern)]),
      "photo": new FormControl(""),

    });
  }
  
  onSignup()
  {
    // upload the photo into s3 bucket
    let photo_url = "";
    
    const values = this.form.value;
    const doctor:Doctor = new Doctor(
      values['name'],values['email'],
      values['phone'], values['speciality'],
      values['education'],values['fees'], photo_url);
    doctor.password = values['password'];
    
    const observable = this.authService.doctorSignUp(doctor);
    observable.subscribe(
      (resp)=>{
        console.log("resp is : ", resp);
        if(resp.status == 201){
          this.errorMsg = "";
          this.successMsg = "Successful SignUp";
          setTimeout(() => {
            this.router.navigate(["/signin"]);
          }, 1000);
        }
        else{
          this.successMsg = "";
          this.errorMsg = "server error please try again latter";
        }
      }, 
      (resp)=>{
        console.log("resp error is : ", resp);
        this.successMsg = "";        
        if(resp.status == 400){
          this.errorMsg = resp.error.message;
        }
        else if(resp.error.status == 500) {
          this.errorMsg = "server error please try again latter";
        }
        else{  // status = 0
          this.errorMsg = "network error"; // it also could be error in parsing the response !!!!!
        }
      });
  }

  isInvalidElement(name:string){
    return this.form.controls[name].invalid && this.form.controls[name].touched;
  }

  getEmailErrorMsg()
  { 
    /** I guess we can use pipes or directives instead */
    if(this.form.controls['email'].errors['required'])
        return "Email is required";
    else if(this.form.controls['email'].errors["email"])
        return "invalid Email";
    return "";
  }

  
  getPassErrorMsg()
  {
    if(this.form.controls['password'].errors['required'])
        return "Password is required";
    return "";
  }
}
