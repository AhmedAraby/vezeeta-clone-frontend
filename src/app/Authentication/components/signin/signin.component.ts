import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/Authentication/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})

export class SigninComponent implements OnInit, OnDestroy{
  form: FormGroup;
  errorMsg = "";
  successMsg = "";

  constructor(private authService:AuthService, private router: Router) { }

  ngOnInit(): void {
    // this method run before rendering the template !?
    this.form = new FormGroup({
      "email": new FormControl("", [Validators.required, Validators.email]), 
      "password": new FormControl("", [Validators.required]),
      "user_type": new FormControl("client", [Validators.required])
    });
  }

  onSignin()
  {
    if(this.form.invalid)
      return ;
      
    const email = this.form.value.email;
    const password = this.form.value.password;
    let observable;

    if(this.form.value.user_type == "client"){
      observable = this.authService.clientSignIn(email, password);
    }
    else if (this.form.value.user_type == "doctor") {
      observable = this.authService.doctorSignIn(email, password);
    }
    else{
      alert("invalid user type");
    }

    observable.subscribe(
      (resp)=>{
        if(resp.status == 200){  // this is the only way we can get in here !!???
          this.errorMsg = "";
          this.successMsg = "logedIn Successfully";
          
          setTimeout(() => {
            if(this.form.value['user_type']=='client')
              this.router.navigate(['/']);
            else 
              this.router.navigate(['doctors', this.authService.getDoctorId(), "profile"]);

          }, 1000);
        }
        else{
          this.errorMsg = "Server Error Please try Again latter";    
        }
    },

    (resp)=>{
      this.successMsg = "";

      if(resp.status == 401){
        this.errorMsg = "Invalid username or password";
      }
      else if(resp.status == 500){
        this.errorMsg = "Server Error Please try Again latter";
      }
      else{ 
        this.errorMsg = "network error";
      }
    });
  }

  // form handling functionality
  isInvalidElement(name:string){
    return this.form.controls[name].invalid && this.form.controls[name].touched;
  }

  getEmailErrorMsg()
  { 
    /** I guess we can use pipes or directives instead */
    if(this.form.controls['email'].errors['required'])
        return "Email is required";
    else if(this.form.controls['email'].errors["email"])
        return "invalid Email";
    return "";
  }

  getPassErrorMsg()
  {
    if(this.form.controls['password'].errors['required'])
        return "Password is required";
    return "";
  }

  ngOnDestroy()
  {

  }
}
