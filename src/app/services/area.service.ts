import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Area } from '../models/area.model';

@Injectable({
  providedIn: 'root'
})
export class AreaService {

  private areasApi: string = "http://localhost:8050/areas";
  constructor(private httpClient: HttpClient) { }
  areaSubject: BehaviorSubject<Area[]|null> = new BehaviorSubject(null);
  getAreas()
  {
    return this.httpClient.get(this.areasApi, 
      {observe:"response", responseType:"json"});
 
  }
}
