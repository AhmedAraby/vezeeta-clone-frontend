import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Schedule } from '../models/schedule.model';
import { ClinicSearchParams } from '../models/clinic-search-params.model';
import { Clinic } from '../models/clinic.model';
import { PaginationInfo } from '../models/pagination-info.model';
import { Reservation } from '../models/reservation.model';

@Injectable({
  providedIn: 'root'
})

export class ClinicService 
{
  
  clinicSelectedSubject: BehaviorSubject<Clinic | null> = new BehaviorSubject(null);
  clinicsApi: string = "http://localhost:8050/clinics";

  constructor(private httpClient: HttpClient) {}

  getClinic(id: string){
    const api = this.clinicsApi + "/" + id;
    return this.httpClient.get<Clinic>(api, 
      {observe:"body", responseType:"json"});
  }

  addClinic(clinic: Clinic){
    return this.httpClient.post<Clinic>(
      this.clinicsApi,
      clinic, 
      {observe:"body", responseType:"json"});
  }
  
  updateClinic(clinic: Clinic){
    return this.httpClient.put(
      this.clinicsApi, 
      clinic, 
      {observe:"body", responseType:"json"});
  }

  getClinics(clinicsApi: string)
  {
    return this.httpClient
    .get(
      clinicsApi, 
      {observe:"response", responseType:"json"})
    .pipe(
      map((resp)=>{
        return resp.body;
      }),
    );
  }

  getClinicSchedule(clinicId:string, weekDay: string)
  {
    const api: string = this.clinicsApi + "/" + clinicId + "/schedule";
    const httpQueryParams = new HttpParams().set("weekday", weekDay);
    return this.httpClient.get(
      api,
      {params:httpQueryParams, observe:"response", responseType:"json"})
      .pipe(
        map((resp)=>{
          return Schedule.createClinicSchedule(resp.body);
        }),
      );
  }

  getClinicReservations(clinicId: string, reservationDate: string)
  {
    const api = this.clinicsApi + "/" + clinicId +"/reservations";
    const httpQueryParams = new HttpParams().set("reservationDate", reservationDate);
    
    return this.httpClient
    .get(api,
      {params:httpQueryParams, observe:"response", responseType:"json"})
      .pipe(
        map((resp: HttpResponse<Reservation[]>)=>{
          let reservations: Reservation[] =[];
          for(let index =0; index<resp.body.length; index++)
            reservations.push(Reservation.createReservation(resp.body[index]));
          return reservations;
        }), 
      );
  }

  emitClinicSelectedSubject(clinic: Clinic | null){
    this.clinicSelectedSubject.next(clinic);
  }

  makeReservation(reservation:Reservation){
    const api = "http://localhost:8050/reservations";
    return this.httpClient.post<Reservation>(
      api,
      reservation,
      {observe:"body",responseType:"json"})
      .pipe(
        map((body: Reservation)=>{
          return Reservation.createReservation(body);
        }), 
      );
  }
}
