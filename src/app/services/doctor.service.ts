import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {DoctorSearchParamsModel } from '../models/doctor-search-params.model';
import { Doctor } from '../models/doctor.model';
import { PaginationInfo } from '../models/pagination-info.model';

@Injectable({
  providedIn: 'root'
})
export class DoctorService {

  doctorApi: string = "http://localhost:8050/doctors"; 
  // ?area=cairo&speciality=dentist&name=abdo&pageNum=1&pageSize=10";
  
  searchParamsSubject: BehaviorSubject<DoctorSearchParamsModel|null> = 
  new BehaviorSubject(null);
  
  searchResultSubject: BehaviorSubject<Doctor[] | null> = 
  new BehaviorSubject(null);

  constructor(private httpClient: HttpClient) { }

  setSearchParams(searchParams: DoctorSearchParamsModel){
    this.emitSearchParamsSubject(searchParams);
  }

  getDoctor(id: string) : Observable<Doctor | HttpResponse<any> | null>
  {
    return this.httpClient.get(
      this.doctorApi + "/" + id,
      {observe:"response", responseType:"json"})
    .pipe(
      map((resp:HttpResponse<Doctor>)=>{
        return Doctor.createDoctor(resp.body);
      })
    );
  }

  getDoctors(
    searchParams: DoctorSearchParamsModel, 
    paginationInfo: PaginationInfo) 
    : Observable<Doctor[] | HttpResponse<any> >  // second type in case of error
  {
    const queryParams =
     new HttpParams()
     .set("name", searchParams.name)
     .set("area", searchParams.area)
     .set("speciality", searchParams.speciality)
     .set("pageNum", paginationInfo.pageNum)
     .set("pageSize", paginationInfo.pageSize);


    // get overloading descripe the type of the response in the body
    return this.httpClient.get<Doctor[]>(
      this.doctorApi, 
      {params:queryParams, observe:"response", responseType:"json"})
      .pipe(
        map((resp)=>{ // only in case of success
          let newBody: Doctor[] = [];
          for(let doctor of resp.body){
            newBody.push(Doctor.createDoctor(doctor));
          }
          return newBody;
        }), 
      );
  }


  updateDoctor(doctor: Doctor){
    return this.httpClient.put(
      this.doctorApi,
      doctor);
  }

  deleteDoctor(id: string){
    return ;
  }

  emitSearchParamsSubject(searchParams: DoctorSearchParamsModel){
    this.searchParamsSubject.next(searchParams);
  }

  emitSearchResultSubject(searchResult: Doctor[] | null){
    this.searchResultSubject.next(searchResult);
  }

}
