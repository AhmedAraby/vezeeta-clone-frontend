import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Speciality } from '../models/speciality.model';
 
@Injectable({
  providedIn: 'root'
})
export class SpecialityService {

  private specialitiesApi: string = "http://localhost:8050/specialities";
  constructor(private httpClient: HttpClient) { }
  areaSubject: BehaviorSubject<Speciality[]|null> = new BehaviorSubject(null);
  getSpecialities()
  {
    return this.httpClient.get(this.specialitiesApi, 
      {observe:"response", responseType:"json"}); 
  }
}
