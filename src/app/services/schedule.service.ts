/**
 * this is non routed component 
 * and it is created very early in angular life time from initialization 
 * as indicated by porvidedIn: route, 
 * 
 * and this means that activatedRoute.snapshot.paramsMap will be empty.
 */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { Schedule } from '../models/schedule.model';

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {

  private scheduleApi: string = "http://localhost:8050/clinics/:id/schedules";
  constructor(
      private httpClient: HttpClient, 
      private activatedRoute: ActivatedRoute) { }


  add(schedule: Schedule)
  {

    const api = this.scheduleApi.replace(":id", schedule.clinicId);
    return this.httpClient.post(
      api, 
      schedule, 
      {observe:"body", responseType:"json"}
    );
  }

  
  update(schedule: Schedule){
    const api = this.scheduleApi.replace(":id", schedule.clinicId);
    return this.httpClient.put(
      api, 
      schedule, 
      {observe:"body", responseType:"json"}
    );
  }

  delete(scheduleId: string, clinicId: string){
    const api = this.scheduleApi.replace(":id", clinicId);
    return this.httpClient.delete(
      api + "/" +scheduleId, 
      {observe:"body", responseType:"json"}
    );
  }


  getSchedules(clinicId: string){
    const api = this.scheduleApi.replace(":id", clinicId);
    return this.httpClient.get<Schedule[]>(
      api,  
      {observe:"body", responseType:"json"}
    ).pipe(
      map((body)=>{
        let schedules: Schedule[] = [];
        for(let schedule of body)
          schedules.push(Schedule.createClinicSchedule(schedule));
        return schedules;
      })
    );
  }
}
