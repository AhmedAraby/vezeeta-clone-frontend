import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ClinicSearchParams } from 'src/app/models/clinic-search-params.model';
import { Clinic } from 'src/app/models/clinic.model';
import { PaginationInfo } from 'src/app/models/pagination-info.model';
import { ClinicService } from 'src/app/services/clinic.service';
 @Component({
  selector: 'app-clinic-list',
  templateUrl: './clinic-list.component.html',
  styleUrls: ['./clinic-list.component.css']
})
export class ClinicListComponent implements OnInit, OnDestroy{
  @Input() clinicSearchParams: ClinicSearchParams;

  nextLink: string = "http://localhost:8050/clinics";
  prevLink: string = null;
  
  clinics: Clinic[] = [];
  clinicsSub = null;

  errorMsg: string = "";

  constructor(private clinicService: ClinicService) {}
  
  ngOnInit(): void {
    this.initNextLink();
    this.getClinics(this.nextLink);
  }

  getClinics(api: string){
    this.clinicsSub = this.clinicService.getClinics(api)
    .subscribe(
      (body)=>{
        this.extendClinics(body['clinics'])
        this.nextLink = body['clinicLinks']['nextLink'];
        this.prevLink = body['clinicLinks']['prevLink'];
        
        
        // #1 
        // if this function executed sync, then this will cuz on working on null variable because 
        // it will execute before assigning value to the clinicsSub variable
        this.clinicsSub.unsubscribe(); 
      }, 
      (resp)=>{
        if(resp.status == 404)
          this.errorMsg = "No More results";
        else 
          this.errorMsg = "Server Error";
        this.clinicsSub.unsubscribe();  // look at #1
      },
    );
  } 

  onClinicClick(index: number){
    this.clinicService.emitClinicSelectedSubject(this.clinics[index]);
  }

  removeSelectedClinic(){
    this.clinicService.emitClinicSelectedSubject(null);
  }

  onNextClick(){
    if(this.nextLink == null) return;
    this.getClinics(this.nextLink);
    this.removeSelectedClinic();
  }

  onPrevClick(){
    if(this.prevLink == null) return ;
    this.getClinics(this.prevLink);
    this.removeSelectedClinic();
  }

  initNextLink(){
    this.nextLink = this.nextLink +
    "?doctorId=" + this.clinicSearchParams.doctorId +
    "&area=" + this.clinicSearchParams.area + 
    "&pageNum=1&pageSize=2";  
  }

  extendClinics(clinics){
    this.clinics = [];
    for(let i=0; i<clinics.length; i++)
      this.clinics.push(Clinic.createClinic(clinics[i]));
  }

  ngOnDestroy(){
    if(this.clinicsSub)
      this.clinicsSub.unsubscribe();
  }

}
