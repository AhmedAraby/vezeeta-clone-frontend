import { Component, Input, OnInit } from '@angular/core';
import { Clinic } from 'src/app/models/clinic.model';

@Component({
  selector: 'app-clinic-card',
  templateUrl: './clinic-card.component.html',
  styleUrls: ['./clinic-card.component.css']
})
export class ClinicCardComponent implements OnInit {
  @Input() clinic: Clinic;
  clinicPhotoPlaceHolder: string 
  = "https://www.foley.com/en/-/media/images/insights/publications/2018/08/cms-continues-to-tighten-the-belt-on-hospital-offc" 
  + "/imageoverridefortwitter/twitterinstream_tall__hospitalwaitingroom.jpg";
  
  constructor() { }

  ngOnInit(): void {
  }

}
