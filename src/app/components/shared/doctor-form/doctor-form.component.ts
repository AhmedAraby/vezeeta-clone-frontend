/**
 * this component is supposed to work only for 
 * doctor users.
 */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/Authentication/services/auth.service';
import { Doctor } from 'src/app/models/doctor.model';
import { User } from 'src/app/models/user.model';
import { DoctorService } from 'src/app/services/doctor.service';
import { ValidatorsUtils } from 'src/app/utils/validatorsUtils';

@Component({
  selector: 'app-doctor-form',
  templateUrl: './doctor-form.component.html',
  styleUrls: ['./doctor-form.component.css']
})

export class DoctorFormComponent implements OnInit, OnDestroy 
{
  viewProfileMode: boolean = false;
  formEnabled:boolean = true;

  doctor: Doctor = null;
  doctorSub = null;

  form: FormGroup;
  // patterns

  phonePattern:string = "^0{1}[0-9]{10}$"; 
  feesPattern:string = "^[1-9]+[0-9]*";
  // password
  passwordP_oneOrMoreChar = ".*[a-zA-Z]+.*";
  passwordP_oneOrMoreNumber = ".*[0-9]+.*";
  
  // messages 
  successMsg = "";
  errorMsg = "";

  constructor(
    private router: Router, 
    private activatedRoute:ActivatedRoute,
    private authService: AuthService, 
    private doctorService: DoctorService)
  {    
    if(this.router.url != "/signup/doctor") // viewProfileMode
      this.viewProfileMode = true;
  }

  ngOnInit(): void 
  {
    this.form = new FormGroup({
      "name": new FormControl("", [Validators.required]),
      "phone": new FormControl("", [Validators.required, Validators.pattern(this.phonePattern)]),
      "email": new FormControl("", [Validators.required, Validators.email]), 
      
      "password": new FormControl("", 
      [
        Validators.required, 
        Validators.minLength(10), 
        Validators.pattern(this.passwordP_oneOrMoreChar), 
        Validators.pattern(this.passwordP_oneOrMoreNumber)
      ]), 
      "newPassword": new FormControl("", 
      [
        Validators.minLength(10), 
        Validators.pattern(this.passwordP_oneOrMoreChar), 
        Validators.pattern(this.passwordP_oneOrMoreNumber)
      ]), 
      "newPasswordConfirm": new FormControl(""),
      "education": new FormControl("", [Validators.required]),
      "speciality": new FormControl("", [Validators.required]),
      "fees": new FormControl("", [Validators.required, Validators.pattern(this.feesPattern)]),
      "photo": new FormControl(""),

    },
     [ // validators on the formGroup level to do cross formControl Validation
      ValidatorsUtils.passwordMatch("newPassword", "newPasswordConfirm"),
    ]);
    
    if(this.viewProfileMode){
      this.getDoctor();
      this.toggleFormState();
    }
  }

  getDoctor()
  {
    // viewProfileMode function
    let doctorId: string = this.activatedRoute.snapshot.paramMap.get("id");
    this.doctorSub = this.doctorService.getDoctor(doctorId).subscribe(
      (doctor:Doctor)=>{
      this.doctor = doctor;
      this.fillFormData(doctor);
    },
    (resp)=>{
      if(resp.status == 404)
        this.errorMsg = "Doctor do not exist";
      else if(resp.status == 500)
        this.errorMsg = "Server Error";
      else 
        this.errorMsg = "Network Error / Application Error";
    });
  }

  fillFormData(doctor:Doctor)
  {
    // viewProfileMode function
    for(let propName in doctor){
      if(!(propName in this.form.controls))
        continue;
      let value = doctor[propName];
      this.form.controls[propName].setValue(value);
    }
  }

  onFormSubmit()
  {
    const doctor: Doctor = Doctor.createDoctor_local(this.form); 
    doctor.photoUrl = "";
    doctor.id = this.doctor.id;

    if(this.viewProfileMode)
      this.doctorUpdate(doctor);
    else
      this.doctorSignUp(doctor);
  }

  doctorUpdate(doctor:Doctor)
  {
    // viewProfileMode function
    this.doctorService.updateDoctor(doctor).subscribe(
      ()=>{
        this.errorMsg = "";
        this.successMsg = "Successful Update";
        setTimeout(() => {  // make message disapper
          this.successMsg = "";
        }, 2000);
        this.toggleFormState();
      }, 
      (resp)=>{
        this.successMsg = "";        
        if(resp.status == 400) // invalid user input
          this.errorMsg = resp.error.message;
        else if(resp.status == 403)
          this.errorMsg = resp.error.message;
        else if(resp.error.status == 500) 
          this.errorMsg = "server error please try again latter";
        else  // status = 0
          this.errorMsg = "network error / application error"; // it also could be error in parsing the response !!!!!
      }
    )
  }

  toggleFormState()
  {
    // viewProfileMode function
    if(this.doctor == null && this.formEnabled == false) return ; // doctor not loaded yet
    for(let controlName in this.form.controls){
      const control = this.form.controls[controlName];
      if(control.disabled)
        control.enable();
      else
        control.disable();
    }
    this.formEnabled = !this.formEnabled;
  }

  isInvalidElement(name:string){
    // console.log("element is : ", name, " ", this.form.controls[name].invalid && this.form.controls[name].touched);
    return this.form.controls[name].invalid && this.form.controls[name].touched;
  }
  doctorSignUp(doctor:Doctor){
    // ignore it for now
  }


  ngOnDestroy(){
    if(this.doctorSub !=null)
        this.doctorSub.unsubscribe();
  }

}
