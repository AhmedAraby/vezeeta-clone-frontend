import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'error-message',
  templateUrl: './error-message.component.html',
  styleUrls: ['./error-message.component.css']
})
export class ErrorMessageComponent implements OnInit {
  @Input() public inputErrorMsg: string;   
  directiveErrorMsg: String; // set through the directive programetically, to avoid wired behaviour about when each of them will be assigned 

  constructor() {}

  ngOnInit(): void {
    // even here @Input may be not set as we set it using directive programetically
  }

}
