import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation-modal.component.html',
  styleUrls: ['./confirmation-modal.component.css']
})
export class ConfirmationModalComponent implements OnInit {

  @Input() title: string;
  @Input() message: string;
  @Output() response: EventEmitter<boolean> = new EventEmitter();

  constructor(public modal: NgbActiveModal) {}  // to access  Modal 

  ngOnInit(): void {
  }

  onCancle(){
    this.response.emit(false);
    this.modal.dismiss('cancel click')
  }
  onOk(){
    this.response.emit(true);
    this.modal.close("using ok");
  }

}
