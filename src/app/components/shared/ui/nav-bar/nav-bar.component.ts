import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/Authentication/services/auth.service';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  user: User;
  constructor(private authService: AuthService) { 
    authService.userStateUpdate.subscribe((user)=>{
      this.user = user;
    })
  }

  ngOnInit(): void {
  }

  isLogedIn(): boolean{
    if(this.user)
      return true;
    return false;
  }

  isDoctor():boolean{
    if(!this.user) return false;
    
    return this.user.userType == "doctor";
  }

  isClient():boolean{
    if(!this.user) return false;

    return this.user.userType == "client";
  }

  onLogOutClick(){
    this.authService.logOut();
  }
}
