import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ClinicSearchParams } from 'src/app/models/clinic-search-params.model';
import { Clinic } from 'src/app/models/clinic.model';
import { ClinicService } from 'src/app/services/clinic.service';

@Component({
  selector: 'app-doctor-board',
  templateUrl: './doctor-profile.component.html',
  styleUrls: ['./doctor-profile.component.css']
})

export class DoctorProfileComponent implements OnInit, OnDestroy {

  clinicSearchParams: ClinicSearchParams;
  doctorId: string;

  viewClinicMode:boolean = true;
 
  // subs 
  clinicSub = null;

  constructor(
    private activatedRoute: ActivatedRoute, 
    private clinicService: ClinicService, 
    private router: Router)
  { 
    this.doctorId = this.activatedRoute.snapshot.paramMap.get("id");
    this.clinicSearchParams = new ClinicSearchParams(this.doctorId, null);
    this.clinicSub = this.clinicService.clinicSelectedSubject.subscribe(
      (clinic: Clinic | null)=>{
        if(clinic == null)
          return ;
        this.router.navigate(["clinics", clinic.id, "profile"]);
      }
    )
  }

  toggleClinicView(){
    this.viewClinicMode = !this.viewClinicMode;
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void 
  {
    if(this.clinicSub != null)
      this.clinicSub.unsubscribe();
  }

}
