import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/Authentication/services/auth.service';
import { Clinic } from 'src/app/models/clinic.model';
import { ClinicService } from 'src/app/services/clinic.service';
import { ValidatorsUtils } from 'src/app/utils/validatorsUtils';

@Component({
  selector: 'app-clinic-form',
  templateUrl: './clinic-form.component.html',
  styleUrls: ['./clinic-form.component.css']
})
export class ClinicFormComponent implements OnInit, OnDestroy{

  // default setting for viewing an already exist clinic
  viewClinicMode=true;
  formEnabled=true;

  form: FormGroup = new FormGroup({});
  clinic: Clinic = null;

  // messages 
  errorMsg ="";
  successMsg = "";

  // subs
  clinicSub=null;

  constructor(
    private router: Router,
    private activatedRoute:ActivatedRoute,
    private clinicService: ClinicService,
    private authService: AuthService)
  { 
    // detect which case the form component should be in
    if(this.router.url.split('/')[1] =="doctors") // create new clinic
      this.viewClinicMode = false;
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      "name": new FormControl("", [Validators.required]), 
      "location": new FormControl("", [Validators.required]),
      "address": new FormControl("", [Validators.required]), 
      "photo": new FormControl(""), 
    });

    if(this.viewClinicMode)
      this.initComponent_updateClinicCase();
  }

  initComponent_updateClinicCase()
  {
    const clinicId: string = this.activatedRoute.snapshot.paramMap.get("id");      
    this.toggleFormState();
    
    this.clinicService.getClinic(clinicId).subscribe(
      (clinic: Clinic)=>{
        this.clinic = clinic;
        this.fillForm();
      },

      (resp)=>{
        if(resp.status = 404)
          this.errorMsg = "failed to load clinic, clinic do not exist";
        else if(resp.status == 500)
          this.errorMsg = "failed to load clinic, server error";
        else if(resp.status == 0)
          this.errorMsg = "failed to load clinic, application / network error";
        else  // unAuthorized or other cases
          this.errorMsg = "unable to load clinic with id " + clinicId;
      }
    );

  }

  fillForm(){
    if(this.clinic == null)
      return ;
      
    for(let key of Object.keys(this.clinic)){
      if(key in this.form.controls){
        this.form.controls[key].setValue(this.clinic[key]);
      }
    }
  }

  toggleFormState()
  {
    if(this.formEnabled == false && this.clinic == null) return;
    this.formEnabled = !this.formEnabled;
    for(let controlKey in this.form.controls){
      const control = this.form.controls[controlKey];
      if(control.disabled)
        control.enable();
      else 
        control.disable();
    }
  }

  onFormSubmit(){
    if(this.viewClinicMode)
      this.updateClinic();
    else 
      this.createClinic();
  }


  updateClinic(){
    const newClinic:Clinic = Clinic.createClinic_local(this.form);
    newClinic.id = this.clinic.id;
    newClinic.doctorId = this.clinic.doctorId;
    const newImageUrl = null; // S3 bucket link
    newClinic.imageUrl = newImageUrl || this.clinic.imageUrl;
    this.clinicService.updateClinic(newClinic).subscribe(
      ()=>{
        this.successMsg = "clinic is updated successfully";
      }, 
      (resp)=>{
        console.log("error resp is : ", resp);
        this.errorMsg = "failed to update clinic ";
        if(resp.status == 500)
          this.errorMsg += " Due to Server Error";
        else if(resp.status == 0)
          this.errorMsg += " Due to Application Error / Network Error";
        else if(resp.status == 400){
          // invlaid data, but how could the user bypass the frontEnd valdiation
          this.errorMsg +=", in correct data";
        }
        else
          this.errorMsg = "unable to update clinic";
      }
    )
  }

  createClinic(){
    const clinic: Clinic = Clinic.createClinic_local(this.form);
    clinic.imageUrl  = ""; // after uploading the image to s3 pucket.
    clinic.doctorId = this.authService.getDoctorId(); 

    this.clinicSub = this.clinicService.addClinic(clinic)
    .subscribe(
      (clinic: Clinic)=>{
        this.successMsg = "Clinic with id " + clinic.id + " Created Successfully";
        setTimeout(() => {
          this.successMsg = "";
        }, 2000);
      }, 
      (resp)=>{
        if(resp.status == 500)
          this.errorMsg = "server error";
        else 
          this.errorMsg = "NetWork Error / Application error";
      }
    );
  }


  isInvalidElement(name:string){
    return this.form.controls[name].invalid && this.form.controls[name].touched;
  }

  ngOnDestroy(){
    if(this.clinicSub)
      this.clinicSub.unsubscribe();
  }
}
