import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Schedule } from 'src/app/models/schedule.model';
import { ClinicService } from 'src/app/services/clinic.service';
import { ScheduleService } from 'src/app/services/schedule.service';
import { ValidatorsUtils } from 'src/app/utils/validatorsUtils';

@Component({
  selector: 'app-schedule-card',
  templateUrl: './schedule-card.component.html',
  styleUrls: ['./schedule-card.component.css']
})
export class ScheduleCardComponent implements OnInit, OnDestroy {

  @Input() clinicSchedule: Schedule = null;
  @Input() weekday: string;
  scheduleExist: boolean = false;
  form: FormGroup;

  // messages 
  errorMsg: string = "";
  successMsg: string = "";

  // subs 
  addScheduleSub = null;

  // constraints 
  numberPattern: string = "^[1-9]+[0-9]*$";

  constructor(
    private scheduleService:ScheduleService, 
    private clinicService: ClinicService, 
    private activatedRoute: ActivatedRoute) 
  { 
    // we need to add cross conntrol custom validator 
    this.form = new FormGroup({
      "openFrom":new FormControl("", [Validators.required]), 
      "closeAt": new FormControl("", [Validators.required]), 
      "appointmentSpan": new FormControl("", [Validators.required, Validators.pattern(this.numberPattern),Validators.max(60)])
    }, [ValidatorsUtils.time2AfterTime1("openFrom", "closeAt")]);
  }

  ngOnInit(): void {
    if(this.clinicSchedule!=null)
      this.init_scheduleExist_case();   
  }

  init_scheduleExist_case(){
    this.scheduleExist = true;
    for(let key in this.form.controls){
      this.form.controls[key].setValue(this.clinicSchedule[key]);
    }
  }

  init_scheduleNotExist_case(){
    this.clinicSchedule = null;
    this.scheduleExist = false;
    for(let key in this.form.controls){
      this.form.controls[key].setValue("");
    }
  }
  

  onFormSubmit(){
    if(this.scheduleExist)
      this.updateSchedule();
    else 
      this.addSchedule()
  }

  addSchedule(){
    const schedule: Schedule = Schedule.createClinicSchedule_local(this.form);
    const clinicId: string = this.activatedRoute.snapshot.paramMap.get("id"); 
    schedule.weekday = this.weekday;
    schedule.clinicId = clinicId;

    console.log("schedule is : ", schedule);
    this.addScheduleSub = this.scheduleService.add(schedule).subscribe(
      (schedule: Schedule)=>{
        this.successMsg = "Added successfully";
        this.clinicSchedule = schedule;
        this.scheduleExist = true;
      }, 
      (resp)=>{
        this.errorMsg = "Failed to save Schedule, "
        if(resp.status == 409)
          this.errorMsg += "Invalid Date";
        else if(resp.status == 500)
          this.errorMsg += "Server Error";
        else 
          this.errorMsg += "Application / Network Error";
      }
    ) 
  }

  updateSchedule(){
    const schedule: Schedule = Schedule.createClinicSchedule_local(this.form);
    const clinicId: string = this.activatedRoute.snapshot.paramMap.get("id"); 
    schedule.weekday = this.weekday;
    schedule.clinicId = clinicId;
    schedule.id = this.clinicSchedule.id;

    this.addScheduleSub = this.scheduleService.update(schedule).subscribe(
      ()=>{
        this.successMsg = "updated successfully";
        this.clinicSchedule = schedule;
      }, 
      (resp)=>{
        this.errorMsg = "Failed to update Schedule, "
        if(resp.status == 409)
          this.errorMsg += "Invalid Date";
        else if(resp.status == 500)
          this.errorMsg += "Server Error";
        else 
          this.errorMsg += "Application / Network Error";
      }
    ) 
  }
 
  deleteSchedule(){
    this.scheduleService.delete(this.clinicSchedule.id, this.clinicSchedule.clinicId).subscribe(
      ()=>{
        this.successMsg = "Deleted Successfully";
        this.init_scheduleNotExist_case();
      }, 
      (resp)=>{
        this.errorMsg = "Failed to delete Schedule, "
        if(resp.status == 500)
          this.errorMsg += "Server Error";
        else 
          this.errorMsg += "Application / Network Error";
      }
    )
  }


  isInvalidElement(name:string){
    // console.log("element is : ", name, " ", this.form.controls[name].invalid && this.form.controls[name].touched);
    return this.form.controls[name].invalid && this.form.controls[name].touched;
  }

  ngOnDestroy(){
    if(this.addScheduleSub !=null)
      this.addScheduleSub.unsubscribe();
  }

}
