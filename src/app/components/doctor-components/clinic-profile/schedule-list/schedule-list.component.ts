import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Schedule } from 'src/app/models/schedule.model';
import { ScheduleService } from 'src/app/services/schedule.service';
import { DateUtils } from 'src/app/utils/dateUtils';

@Component({
  selector: 'app-schedule-list',
  templateUrl: './schedule-list.component.html',
  styleUrls: ['./schedule-list.component.css']
})
export class ScheduleListComponent implements OnInit {

  schedulesMap = {};
  schedulesArr = [];
  constructor(
    private activatedRoute: ActivatedRoute, 
    private scheduleService: ScheduleService
    ) 
  { 
    // will preserve order of insertion, and DateUtils.weekdays has the right order we need
    for(let key in DateUtils.weekdays){ 
      const weekDay: string = DateUtils.weekdays[key];
      this.schedulesMap[weekDay] = null;   
    } 
  }

  ngOnInit(): void
  {
    const clinicId: string = this.activatedRoute.snapshot.paramMap.get("id");
    this.scheduleService.getSchedules(clinicId).subscribe(
      (schedules: Schedule[])=>{
        for(let schedule of schedules)
          this.schedulesMap[schedule.weekday] = schedule;
        this.objectToArray();
      }, 
      (resp)=>{
        alert("failed")
      }

    );
  }

  objectToArray(){
    // angular can not loop over object in the template, it has to be an array
    for(let key in this.schedulesMap)
      this.schedulesArr.push({
        weekday:key,
        schedule:this.schedulesMap[key]});
  }
}
