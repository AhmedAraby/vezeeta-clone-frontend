import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Area } from 'src/app/models/area.model';
import { DoctorSearchParamsModel } from 'src/app/models/doctor-search-params.model';
import { Speciality } from 'src/app/models/speciality.model';
import { AreaService } from 'src/app/services/area.service';
import { DoctorService } from 'src/app/services/doctor.service';
import { SpecialityService } from 'src/app/services/speciality.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit, OnDestroy{
  form: FormGroup;

  // data 
  areas: Area[] = [];
  specialities: Speciality[] = [];
  
  // subs
  areasSubscription = null;
  specialitiesSubscription = null;
 
  // errorMsgs 
  areasErrorMsg: string = "";
  specialitiesErrorMsg: string = "";
  
  constructor(
    private doctorSearchService: DoctorService, 
    private areaService:AreaService, 
    private specialityService: SpecialityService)
  { 
   
    this.form = new FormGroup({
      area: new FormControl("0"), 
      speciality: new FormControl("0"), 
      name: new FormControl(""), 
    });    
    this.fillAreas();
    this.fillSpecialities();
  }

  ngOnInit(): void {
  }

  fillSpecialities(){
    const specialitiesObservable = this.specialityService.getSpecialities();
    this.specialitiesSubscription = specialitiesObservable.subscribe(
      (resp)=>{
        if(resp.status == 200){
          let objKeys = Object.keys(resp.body);
          for(let key of objKeys){
            let speciality = resp.body[key];
            this.specialities.push(new Area(speciality['id'], speciality['name']));
          }
        }
        else{
          this.specialitiesErrorMsg = "error happens during loading the page";
        }
      },

      (resp)=>{
        this.specialitiesErrorMsg = "error happens during loading the page";
      }
    );
  }

  fillAreas(){
    const areasObservable = this.areaService.getAreas();
    this.areasSubscription = areasObservable.subscribe(
      (resp)=>{
        if(resp.status == 200){
          let objKeys = Object.keys(resp.body);
          for(let key of objKeys){
            let area = resp.body[key];
            this.areas.push(new Area(area['id'], area['name']));
          }
        }
        else{
          this.areasErrorMsg = "error happens during loading the page";
        }
      },

      (resp)=>{
        this.areasErrorMsg = "error happens during loading the page";
      }
    );
  }
  onFormSubmit(){
    // console.log("form values : ", this.form.value);
    
    const values = this.form.value;
    
    const name =  values['name'];
    let area = values['area'];
    let speciality = values['speciality'];
    
    // check if user did not make a choise
    if(speciality == "0")
      speciality ="";
    if(area == "0")
      area = "";

    const searchParams = new DoctorSearchParamsModel(name, area, speciality);
    this.doctorSearchService.setSearchParams(searchParams);
   }

   
  ngOnDestroy(){
    this.areasSubscription.unsubscribe();
  }


}
