import { HttpResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DoctorSearchParamsModel } from 'src/app/models/doctor-search-params.model';
import { Doctor } from 'src/app/models/doctor.model';
import { PaginationInfo } from 'src/app/models/pagination-info.model';
import { DoctorService } from 'src/app/services/doctor.service';

@Component({
  selector: 'app-doctors-list',
  templateUrl: './doctors-list.component.html',
  styleUrls: ['./doctors-list.component.css']
})
export class DoctorsListComponent implements OnInit, OnDestroy {

  doctors: Doctor[] = [];
  paginationInfo: PaginationInfo = new PaginationInfo(1, 10);
  errorMsg: string = "";

  doctorSearchParams: DoctorSearchParamsModel;
  doctorSearchParamsSub = null;
  constructor(
    private doctorService: DoctorService, 
    private router: Router) 
  { 
    this.doctorService.searchParamsSubject
    .subscribe(
      (doctorSearchParams: DoctorSearchParamsModel | null)=>{
        if(doctorSearchParams == null) return ;
        this.doctorSearchParams = doctorSearchParams;
        this.getDoctors(this.doctorSearchParams);
        // console.log("docList: search params are : ", this.doctorSearchParams);
      });
  }

  getDoctors(doctorSearchParams: DoctorSearchParamsModel){
    let obs = this.doctorService
    .getDoctors(doctorSearchParams, this.paginationInfo);
    
    this.doctorSearchParamsSub = 
    obs
    .subscribe(
      (doctors: Doctor[])=>{
        this.doctors = doctors;
      },
       
      (resp: HttpResponse<any>)=>{
        console.log("doctors-List: resp error is :", resp);
        
        if(resp.status == 400){
          this.errorMsg = "Application Error";
        }
        else if(resp.status == 403){
          this.router.navigate(["signin"]);
        }
        else{
          this.errorMsg = "Server Error";
        }
      }
    );
  }

  onDoctorClick(id: number){
    this.router.navigate(["doctors", id, "booking"], {queryParams:{area:this.doctorSearchParams.area}});
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void{
    if(this.doctorSearchParamsSub !=null)
      this.doctorSearchParamsSub.unsubscribe();
  }
}
