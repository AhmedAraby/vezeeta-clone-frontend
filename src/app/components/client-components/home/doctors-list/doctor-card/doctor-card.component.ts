import { Component, Input, OnInit } from '@angular/core';
import { Doctor } from 'src/app/models/doctor.model';

@Component({
  selector: 'app-doctor-card',
  templateUrl: './doctor-card.component.html',
  styleUrls: ['./doctor-card.component.css']
})
export class DoctorCardComponent implements OnInit {

  photoPlaceHolder: String = "https://www.clearmountainbank.com/wp-content/uploads/2020/04/male-placeholder-image.jpeg";
  @Input() doctor:Doctor = null;
  constructor() { }

  ngOnInit(): void {
  }

}
