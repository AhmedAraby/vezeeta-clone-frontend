import { Component, Input, NgModuleRef, OnDestroy, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from 'src/app/Authentication/services/auth.service';
import { Appointment } from 'src/app/models/appointment.model';
import { Schedule } from 'src/app/models/schedule.model';
import { Reservation } from 'src/app/models/reservation.model';
import { User } from 'src/app/models/user.model';
import { ClinicService } from 'src/app/services/clinic.service';
import { DateUtils } from 'src/app/utils/dateUtils';
import { ConfirmationModalComponent } from '../../../../shared/ui/confirmation-modal/confirmation-modal.component';
import { AppointmentStrPipe } from 'src/app/pipes/appointment-str.pipe';

@Component({
  selector: 'app-appointment',
  templateUrl: './appointment.component.html',
  styleUrls: ['./appointment.component.css']
})
export class AppointmentComponent implements OnInit, OnDestroy {

  @Input() clinicSchedule: Schedule; // for the cur day
  @Input() dayDate: Date;
  @Input() clinicId:string;

  appointments: Appointment[] = [];
  reservations: Reservation[] = []; // appointments that already reserved and not available
  available: boolean[] = [];
  user: User = null;

  // subs 
  reservationSub = null;
  makeReservationSub = null;

  constructor(
    private clinicService: ClinicService, 
    private authService: AuthService, 
    private modelService: NgbModal) 
  {
    this.authService.userStateUpdate.subscribe((user)=> this.user = user ); 
  }

  ngOnInit(): void {
    console.log("clinic Schedule is : ", this.clinicSchedule);
    
    this.appointments = this.clinicSchedule.getAppointments();

    let dateStr = DateUtils.getDateStr(this.dayDate);    
    this.reservationSub = this.clinicService.getClinicReservations(
      this.clinicId,
      dateStr)
      .subscribe(
        (reservations: Reservation[])=>{
          console.log("appointment - reservations ", reservations);
          this.reservations = reservations;

          this.initAvailable(this.appointments.length);
          this.disableReservedAppointments();
        }, 
        (resp)=>{
          console.log("appointment error resp : ", resp);
        }
      );
  }

  initAvailable(length: number){
    while(length--){
      this.available.push(true);
    }
  }

  disableReservedAppointments()
  {
    this.reservations.sort((resA, resb)=>{
      if(resA.startAt < resb.startAt)
        return -1;
      else if(resA.startAt > resb.startAt)
        return 1;
      return 0;
    });

    let appointmentIndex = 0;
    let reservationIndex = 0;
    
    // 2 pointer(index) based algorithm
    while(true)
    {  
      if(appointmentIndex == this.appointments.length ||
        reservationIndex == this.reservations.length){
        break;
      }

      if(this.reservations[reservationIndex].startAt < this.appointments[appointmentIndex].getStartAt())
        reservationIndex +=1;
      else if(this.reservations[reservationIndex].startAt > this.appointments[appointmentIndex].getStartAt())
        appointmentIndex +=1;
      else{
        this.available[appointmentIndex] = false;
        appointmentIndex +=1;
        reservationIndex +=1;
      }
    }
  }

  onAppointmentClick(index:number){
    if(this.available[index] == false) 
      return ;
    if(this.user == null)
      return ;
    // render confirmation modal
    const modalRef = this.modelService.open(ConfirmationModalComponent);
    modalRef.componentInstance.title = "Reservation Confirmation";
    modalRef.componentInstance.message = "Confirm Appointment at " + 
                                          new AppointmentStrPipe().transform(this.appointments[index]);
    modalRef.componentInstance.response.subscribe(
      (confirm:boolean)=>{
        if(confirm == true)
          this.makeReservation(index);
      }
    );
  }

  makeReservation(index:number)
  {
    // we need to extract the logic of reservation object creation from this method, for better clarity
    const endAt: string = DateUtils.addMs(
      this.appointments[index].getStartAt(),
      this.clinicSchedule.appointmentSpan * 60000);  

    const reservation = new Reservation(
      this.appointments[index].getStartAt(), endAt,
      DateUtils.getDateStr(this.dayDate), this.clinicId,
      this.user.id.toString());

    console.log("reservation is ", reservation);
    this.makeReservationSub = this.clinicService.makeReservation(reservation)
    .subscribe(
      (savedReservation: Reservation)=>{
        console.log("saved Reservation : ", savedReservation);
        this.available[index] = false;
      }, 
      (resp)=>{
        console.log("save reservation error resp : ", resp);
      }
    );
  }
  
  ngOnDestroy(){
    if(this.reservationSub != null)
      this.reservationSub.unsubscribe();
    if(this.makeReservationSub != null)
      this.makeReservationSub.unsubscribe();
  }

}