import { Component, OnDestroy, OnInit } from '@angular/core';
import { Schedule } from 'src/app/models/schedule.model';
import { Clinic } from 'src/app/models/clinic.model';
import { ClinicService } from 'src/app/services/clinic.service';
import { DateUtils } from 'src/app/utils/dateUtils';

@Component({
  selector: 'app-appointment-list',
  templateUrl: './appointment-list.component.html',
  styleUrls: ['./appointment-list.component.css']
})
export class AppointmentListComponent implements OnInit, OnDestroy {
  clinic: Clinic = null; // selected from the clinic list and emited throw the clinic service
 
  numOfDays = 3; // today, tomorrow, day after tomorrow 
  noScheduleCnt = 0; // numebr of days that has no schedule

  // data 
  clinicSchedules: Schedule[] = [];
  dayDateArr: Date[] = [];

  // subs
  clinicScheduleSubs = [];
  clinicSub = null;

  // message handling
  message: string = "";
  messageClass: string = "";

  constructor(private clinicService: ClinicService) {
    this.getClinic();
  }

  ngOnInit(): void {
  }

  getClinic(){
    if(this.clinicSub != null) 
      this.clinicSub.unsubscribe(); // remove subscribtion of the previously selected clinic if any
    
    this.clinicSub = this.clinicService
    .clinicSelectedSubject
    .subscribe( // callbacks will run synchronously
      (clinic: Clinic)=>{
        this.clinic = clinic;
        if(clinic == null)
          this.reInit();
        else
          this.registerGetSchedule();        
      });
  }


  reInit(){
    // reinitialize
    this.noScheduleCnt = 0;
    this.message = "";
    this.messageClass = "";

    this.dayDateArr = [];
    this.clinicScheduleSubs = [];
    this.clinicSchedules = [];
  }

  registerGetSchedule(){
    this.reInit();

    for(let index=0; index<this.numOfDays; index++){
      let date = new Date();
      date.setDate(date.getDate() + index);
      
      this.dayDateArr.push(date);
      this.clinicSchedules.push(null);
      this.clinicScheduleSubs.push(null);      
      this.getSchedule(index);
    }
  }


  getSchedule(index: number){
    // get schedule 
    this.clinicScheduleSubs[index] = this.clinicService
    .getClinicSchedule(this.clinic.id, DateUtils.getWeekDayName(this.dayDateArr[index]))
    .subscribe(

      (schedule: Schedule)=>{
        // make use of closure to access "index"
        this.clinicSchedules[index] = schedule;
        this.clinicScheduleSubs[index].unsubscribe(); // is this safe !??        
      }, 

      (resp)=>{ // failure case
        // should I use the error message from the response !!???
        if(resp.status == 404){
          this.noScheduleCnt +=1;
          if(this.noScheduleCnt == this.numOfDays){
            this.message = "Clinic has no Registered Schedule";
            this.messageClass = "alert-warning";
          }
        }
        else if(resp.status == 500){
          this.message = "Server Error";
          this.messageClass = "alert-danger";
        }
        else{
          this.message = "Application Error";
          this.messageClass = "alert-danger";
        }
      }
    )
  }

  ngOnDestroy(){
    console.log("destory appointment list");
    if(this.clinicSub !=null)
      this.clinicSub.unsubscribe();
    for(let index=0; index<this.numOfDays; index++)
      if(this.clinicScheduleSubs[index] != null)
        this.clinicScheduleSubs[index].unsubscribe();

    // remove selectd clinic
    //this.clinicService.clinicSelectedSubject.next(null);  // no this should not be the responsibility of this component.
  }

}
