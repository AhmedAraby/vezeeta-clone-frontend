import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClinicSearchParams } from 'src/app/models/clinic-search-params.model';
import { Doctor } from 'src/app/models/doctor.model';
import { DoctorService } from 'src/app/services/doctor.service';

@Component({
  selector: 'app-doctor',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css']
})

export class BookingComponent implements OnInit, OnDestroy {
  doctor: Doctor = null;
  doctorId: string;
  area: string;
  doctorSub = null;

  constructor(private doctorService: DoctorService, private activatedRoute: ActivatedRoute) {
    /**
     * should we retrive it from cahsed doctors
     * or just query the DB.
     */
     this.doctorId = this.activatedRoute.snapshot.paramMap.get('id');
     this.area = this.activatedRoute.snapshot.queryParamMap.get('area');
    //  console.log("id is : ", this.id, " area is : ", this.area)
     this.doctorSub = this.doctorService.getDoctor(this.doctorId)
     .subscribe(
       (doctor: Doctor)=>{
         this.doctor = doctor;
       }, 
       (resp)=>{
         console.log("doctor-component, resp error is : ", resp);
       }
     );
  }

  getClinicSearchParams(){
    return new ClinicSearchParams(this.doctorId, this.area);
  }
  ngOnInit(): void {
    
  }

  ngOnDestroy(){
    if(this.doctorSub !=null)
      this.doctorSub.unsubscribe();
  }

}
