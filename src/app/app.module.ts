import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule, HTTP_INTERCEPTORS} from "@angular/common/http";
import {ReactiveFormsModule} from "@angular/forms";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ErrorMessageComponent } from './components/shared/ui/error-message/error-message.component';
import { SuccessMessageComponent } from './components/shared/ui/success-message/success-message.component';
import { NotFoundComponent } from './components/shared/ui/not-found/not-found.component';
import { SigninComponent } from './Authentication/components/signin/signin.component';
import { DoctorSignupComponent } from './Authentication/components/doctor-signup/doctor-signup.component';
import { ClientSignupComponent } from './Authentication/components/client-signup/client-signup.component';
import { ValidationErrorDirective } from './Directives/validation-error.directive';
import { NavBarComponent } from './components/shared/ui/nav-bar/nav-bar.component';
import { SearchComponent } from './components/client-components/home/search/search.component';
import { DoctorCardComponent } from './components/client-components/home/doctors-list/doctor-card/doctor-card.component';
import { DoctorsListComponent } from './components/client-components/home/doctors-list/doctors-list.component';
import { HomeComponent } from './components/client-components/home/home.component';
import { BookingComponent } from './components/client-components/booking/booking.component';
import { ClinicListComponent } from './components/shared/clinic-list/clinic-list.component';
import { ClinicCardComponent } from './components/shared/clinic-list/clinic-card/clinic-card.component';
import { AppointmentListComponent } from './components/client-components/booking/appointment-list/appointment-list.component';
import { AppointmentComponent } from './components/client-components/booking/appointment-list/appointment/appointment.component';
import { WeekDayPipe } from './pipes/week-day.pipe';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppointmentStrPipe } from './pipes/appointment-str.pipe';
import { DoctorFormComponent } from './components/shared/doctor-form/doctor-form.component';
import { DoctorProfileComponent } from './components/doctor-components/doctor-profile/doctor-profile.component';
import { ClinicFormComponent } from './components/doctor-components/clinic-form/clinic-form.component';
import { ClinicProfileComponent } from './components/doctor-components/clinic-profile/clinic-profile.component';
import { AuthInterceptorInterceptor } from './Authentication/services/auth-interceptor.interceptor';
import { ScheduleCardComponent } from './components/doctor-components/clinic-profile/schedule-list/schedule-card/schedule-card.component';
import { ScheduleListComponent } from './components/doctor-components/clinic-profile/schedule-list/schedule-list.component';

@NgModule({
  declarations: [
    AppComponent, 
    ErrorMessageComponent,
    SuccessMessageComponent,
    NotFoundComponent,
    SigninComponent, 
    DoctorSignupComponent, 
    ClientSignupComponent,
    ValidationErrorDirective,
    NavBarComponent,
    SearchComponent,
    DoctorCardComponent,
    DoctorsListComponent, 
    HomeComponent,
    BookingComponent,
    ClinicListComponent,
    ClinicCardComponent,
    AppointmentListComponent, 
    AppointmentComponent,
    WeekDayPipe,
    AppointmentStrPipe,
    DoctorFormComponent,
    DoctorProfileComponent,
    ClinicFormComponent,
    ClinicProfileComponent,
    ScheduleListComponent,
    ScheduleCardComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, 
    HttpClientModule, 
    ReactiveFormsModule,
    NgbModule
  ],
  providers: [
    {provide:HTTP_INTERCEPTORS, useClass:AuthInterceptorInterceptor, multi:true}
  ],
  bootstrap: [AppComponent], 
})
export class AppModule { }
