/**
 * this class has helper static method 
 * for the manipulation we need to do on Dates and Times, 
 * of types JS obejct or string. 
 */
export class DateUtils
{
    static weekdays = {
        0:"sunday", 1:"monday",
        2:"tuesday", 3:"wednesday",
        4:"thursday", 5: "friday",
        6: "saturday", 
    };

    // static weekdaysRank = {
    //     sunday:0, monday:1,
    //     tuesday:2, wednesday:3,
    //     thursday:4, friday:5,
    //     saturday:6, 
    // };

    static make2Digits_str(num:string): string
    {
        if(num.length < 2)
            return "0" + num;
        return num;
    }

    static getDaySection(hours: number)
    {
        if(hours < 12)
            return "AM";
        return "PM";
    }

    static make2Digits(num:number): string
    {
        if(num < 10)
            return "0" + num;
        return num + ""; // for casting
    }

    /**
     * 
     * @param date JS Date object
     * @param separator 
     * @returns string representation of the Date part (year, month, day).
     */
    static getDateStr(date:Date, separator="-")
    {
        
        let dateParts = date.toLocaleDateString().split("/"); // month / day / year
        let dateStr =   dateParts[2] +  // year
                        separator + 
                        this.make2Digits_str(dateParts[0]) +  // month
                        separator + 
                        this.make2Digits_str(dateParts[1]); // day
        return dateStr;
    }
    
    static getTimeStr(date: Date): string{
        return date.toLocaleTimeString().split(" ")[0];
    }

    static getWeekDayName(date: Date): string
    {
        const weekDayNum = date.getDay();
        return DateUtils.weekdays[weekDayNum];
    }

    /**
     * 
     * @param time string of the from hh:mm:ss
     * @param ms millie second to be added to this time
     * @returns (time + ms) in string format, 
     * "+" mean math operation not str concatenation.
     */
    static addMs(time: string, ms:number) : string
    {
        let date = new Date();
        let timeParts = time.split(":"); // hours, minutes, seconds
        date.setHours(
            parseInt(timeParts[0]), 
            parseInt(timeParts[1]),
            parseInt(timeParts[2]),
        );
        date = new Date(date.getTime() + ms);
        
        return DateUtils.getTimeStr(date);
    }

    static to12HoursSystem(hours:number){
        if(hours <=12)
            return hours;
        return hours - 12;
    }

}