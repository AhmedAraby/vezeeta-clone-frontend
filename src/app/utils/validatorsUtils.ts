import { AbstractControl, FormControl, FormGroup, ValidationErrors } from "@angular/forms";
/**
 * it is all about what action triger 
 * the validation process
 */

export class ValidatorsUtils
{
    static passwordMatch(password1Key: string, password2Key: string)
    {
        function innerPasswordMatch(formGroup: FormGroup): ValidationErrors
        {        
            let pass1FormControl: AbstractControl = formGroup.controls[password1Key];
            let pass2FormControl: AbstractControl = formGroup.controls[password2Key];

            // console.log("pass1", pass1FormControl.touched , " ", pass1FormControl.value);
            // console.log("pass2", pass2FormControl.touched , " ", pass2FormControl.value);

            if(pass1FormControl.touched &&
                 pass2FormControl.touched &&
                  pass1FormControl.errors == null)
            {
                let match: boolean = (pass1FormControl.value == pass2FormControl.value);
                if(!match){
                    pass2FormControl.setErrors({"passwordMatch":true});
                    return {"passwordMatch":true};
                }
                
            }
            pass2FormControl.setErrors(null);
            return null;
        }
        return innerPasswordMatch;
    }

    static time2AfterTime1(time1Key: string, time2Key:string)
    {
        function innerCloseAfterOpen(formGroup: FormGroup)
        {
            let time1Control: AbstractControl = formGroup.controls[time1Key];
            let time2Control: AbstractControl = formGroup.controls[time2Key];
            // console.log("time 1 :", time1Control.value, " " , time2Control.value);
            if(time1Control.touched && time2Control.touched && time2Control.value <= time1Control.value){
                time2Control.setErrors({"invalidTime":true});
                return {"invalidTime":true};
            }
            time2Control.setErrors(null);
            return null;
        }

        return innerCloseAfterOpen;
    }
}