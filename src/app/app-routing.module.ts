import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientSignupComponent } from './Authentication/components/client-signup/client-signup.component';
import { DoctorSignupComponent } from './Authentication/components/doctor-signup/doctor-signup.component';
import { SigninComponent } from './Authentication/components/signin/signin.component';
import { AuthGuard } from './Authentication/services/auth.guard';
import { ClientGuard } from './Authentication/services/client.guard';
import { DoctorGuard } from './Authentication/services/doctor.guard';
import { GuestGuard } from './Authentication/services/guest.guard';
import { ClinicProfileComponent } from './components/doctor-components/clinic-profile/clinic-profile.component';
import { ClinicFormComponent } from './components/doctor-components/clinic-form/clinic-form.component';
import { DoctorProfileComponent } from './components/doctor-components/doctor-profile/doctor-profile.component';
import { DoctorFormComponent } from './components/shared/doctor-form/doctor-form.component';
import { BookingComponent } from './components/client-components/booking/booking.component';
import { HomeComponent } from './components/client-components/home/home.component';
import { NotFoundComponent } from './components/shared/ui/not-found/not-found.component';

const routes: Routes = [
  {
    path:"signin",
    component:SigninComponent,
    canActivate:[GuestGuard] 
  },
  
  {
    path:"signup/client",
    component:ClientSignupComponent,
    canActivate:[GuestGuard] 
  },

  {
    path:"signup/doctor",
    component:DoctorSignupComponent,
    canActivate:[GuestGuard] 
  },
  {
    path:"doctors/:id/profile",
    component: DoctorProfileComponent,
    canActivate:[AuthGuard, DoctorGuard],
  },
  {
    path:"doctors/:id/booking",  // this route need to change !!
    component: BookingComponent,
    canActivate:[AuthGuard, ClientGuard],
  },
  {
    path:"clinics/:id/profile", 
    component:ClinicProfileComponent, 
    canActivate:[AuthGuard, DoctorGuard],
  },
  {
    path:"", 
    component:HomeComponent, 
    canActivate:[AuthGuard, ClientGuard],
  }, 
  {
    path:"**", 
    component:NotFoundComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
