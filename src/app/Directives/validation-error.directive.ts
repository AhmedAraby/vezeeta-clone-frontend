import { Directive, ElementRef, Input, OnChanges, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ErrorMessageComponent } from 'src/app/components/shared/ui/error-message/error-message.component';

@Directive({
  selector: '[validationError]'
})
export class ValidationErrorDirective implements OnInit, OnChanges {

  @Input() formControlErrors;
  @Input() errorMsgs;

  constructor(private errorMsgComponent: ErrorMessageComponent) { }

  ngOnInit(){ // we can see the initial value of @Input()  variables for the first time here
  }

  ngOnChanges(changes){  
    // console.log("my errors : ", this.errorMsgs);
    // console.log("control error : ", this.formControlErrors);
    const firstError = Object.keys(this.formControlErrors)[0];
    // console.log("key ", firstError);

    this.errorMsgComponent.directiveErrorMsg = this.errorMsgs[firstError] || "Invalid Input";
  }


}
