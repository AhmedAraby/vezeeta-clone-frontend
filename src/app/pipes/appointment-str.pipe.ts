import { Pipe, PipeTransform } from '@angular/core';
import { Appointment } from '../models/appointment.model';
import { DateUtils } from '../utils/dateUtils';

@Pipe({
  name: 'appointmentStr'
})

export class AppointmentStrPipe implements PipeTransform {

  transform(appointment: Appointment, ...args: unknown[]): string {
    return DateUtils.make2Digits(
      DateUtils.to12HoursSystem(appointment.hours)) + 
    ":" + 
    DateUtils.make2Digits(appointment.minutes) + 
    " " + 
    DateUtils.getDaySection(appointment.hours); // AM or PM
  }

}
