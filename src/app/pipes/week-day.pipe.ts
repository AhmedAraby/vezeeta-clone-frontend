import { Pipe, PipeTransform } from '@angular/core';
import { DateUtils } from '../utils/dateUtils';

@Pipe({
  name: 'weekDayName'
})
export class WeekDayPipe implements PipeTransform {

  transform(curDayDate: Date, ...args: unknown[]): string {
    return DateUtils.getWeekDayName(curDayDate);
  }

}
